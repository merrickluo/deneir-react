import _ from "lodash";

const queryParams = (params) => {
  return _.reduce(params, (r, value, key) => `${r}${key}=${value}&`, "?");
};

export const get = async (host, path = "/", options = {}) => {
  const query = options.params ? queryParams(options.params) : "";
  return fetch(`${host}${path}${query}`, options).then((resp) => resp.json());
};
