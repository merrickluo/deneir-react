import { schema } from "normalizr";

export const category = new schema.Entity("categories");
export const feed = new schema.Entity("feeds", {
  category: category,
});
export const article = new schema.Entity("articles", {
  feed: feed,
});
