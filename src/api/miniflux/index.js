import _ from "lodash";

import { getCredential } from "storage/auth";
import { category, feed, article } from "api/schema";
import { normalize } from "normalizr";
import {
  setCategories,
  setFeedsByCategory,
  setArticlesByCategory,
  setArticlesByFeed,
  setArticles,
} from "stores/ReaderStore";

import { get } from "api/client";

export const AUTH_USE_PASSWORD = 0;
export const AUTH_USE_TOKEN = 1;

const authorize = async (credentials) => {
  let headers = {};
  const { mode, params } = credentials;
  if (mode === AUTH_USE_PASSWORD) {
    headers = {
      Authorization: `Basic ${btoa(`${params.username}:${params.password}`)}`,
    };
  } else if (mode == AUTH_USE_TOKEN) {
    headers = {
      "x-auth-token": params.token,
    };
  } else {
    throw new Error("unknown auth mode");
  }

  const resp = await fetch(`${params.server}/v1/me`, { headers: headers });
  return resp.status === 200;
};

const fetchFeeds = async () => {
  const {
    params: { server, token },
  } = getCredential();
  const resp = await get(server, "/v1/feeds", {
    headers: {
      "x-auth-token": token,
    },
  });
  _.forEach(
    _.groupBy(resp, (item) => item.category.id),
    (value, key) => {
      const { entities, result } = normalize(value, [feed]);
      setFeedsByCategory(key, result, entities);
    }
  );
};

const fetchCategories = async () => {
  const {
    params: { server, token },
  } = getCredential();
  const resp = await fetch(`${server}/v1/categories`, {
    headers: {
      "x-auth-token": token,
    },
  });
  const data = await resp.json();
  const { result, entities } = normalize(data, [category]);
  setCategories(result, entities);
};

const fetchArticlesByCategory = async (categoryId, page) => {
  const {
    params: { server, token },
  } = getCredential();
  const resp = await get(server, "/v1/entries", {
    params: {
      limit: 50,
      category_id: categoryId,
      order: "published_at",
      direction: "desc",
      offset: 50 * page,
    },
    headers: {
      "x-auth-token": token,
    },
  });
  const { total: total, entries: entries } = resp;
  const { result, entities } = normalize(entries, [article]);
  setArticlesByCategory(categoryId, result, entities);
};

const fetchArticlesByFeed = async (feedId, page) => {
  const {
    params: { server, token },
  } = getCredential();
  const resp = await get(server, `/v1/feeds/${feedId}/entries`, {
    params: {
      limit: 50,
      order: "published_at",
      direction: "desc",
      offset: 50 * page,
    },
    headers: {
      "x-auth-token": token,
    },
  });
  const { total: total, entries: entries } = resp;
  const { result, entities } = normalize(entries, [article]);
  setArticlesByFeed(feedId, result, entities);
};

const fetchAllArticles = async (page) => {
  const {
    params: { server, token },
  } = getCredential();
  const resp = await get(server, "/v1/entries", {
    params: {
      limit: 50,
      order: "published_at",
      direction: "desc",
      offset: 50 * page,
    },
    headers: {
      "x-auth-token": token,
    },
  });
  const { total: total, entries: entries } = resp;
  const { result, entities } = normalize(entries, [article]);
  setArticles(result, entities);
};

const fetchArticles = async ({ categoryId, feedId }, page = 0) => {
  if (categoryId) {
    fetchArticlesByCategory(categoryId, page);
  } else if (feedId) {
    fetchArticlesByFeed(feedId, page);
  } else {
    fetchAllArticles(page);
  }
};

const fetchIcon = async (feedId) => {
  const {
    params: { server, token },
  } = getCredential();
  const resp = fetch(`${server}/v1/feeds/${feedId}/icon`, {
    headers: {
      "x-auth-token": token,
    },
  });
  const { data: data } = await resp.json();
  return data;
};

const MinifluxAPI = {
  authorize,
  fetchFeeds,
  fetchArticles,
  fetchIcon,
  fetchCategories,
};

export default MinifluxAPI;
