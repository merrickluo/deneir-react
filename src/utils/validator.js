import validator from "validator";

export const urlValidator = (url) => {
  if (!validator.isURL(url)) {
    throw new Error("not a valid URL");
  }
  return true;
};

export const emptyValidator = (string) => {
  if (validator.isEmpty(string)) {
    throw new Error("must not be empty");
  }
  return true;
};
