export const hasCredential = (service) => {
  return localStorage.getItem(`credentials/${service}`);
};

export const updateCredential = (service, credentials) => {
  localStorage.setItem(`credentials/${service}`, JSON.stringify(credentials));
};

export const getCredential = (service = "miniflux") => {
  return JSON.parse(localStorage.getItem(`credentials/${service}`));
};

export default {
  hasCredential,
  updateCredential,
  getCredential,
};
