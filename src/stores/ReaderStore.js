import { Store } from "pullstate";
import _ from "lodash";

export const ReaderStore = new Store({
  categoryIds: [],
  feedIdsByCategory: {},
  articleIds: {
    byCategory: {},
    byFeed: {},
    byAll: {},
  },
  entities: {
    categories: {},
    feeds: {},
    articles: {},
  },
});

export const setCategories = (categoryIds, entities) => {
  ReaderStore.update((s) => {
    s.entities = _.merge(s.entities, entities);
    s.categoryIds = categoryIds;
  });
};

export const setFeedsByCategory = (categoryId, feedIds, entities) => {
  ReaderStore.update((s) => {
    s.entities = _.merge(s.entities, entities);
    s.feedIdsByCategory[categoryId] = feedIds;
  });
};

export const setArticlesByFeed = (feedId, articleIds, entities) => {
  ReaderStore.update((s) => {
    s.entities = _.merge(s.entities, entities);
    s.articleIds.byFeed[feedId] = _.uniq([...(s.articleIds.byFeed[feedId] || []), ...articleIds]);
  });
};

export const setArticlesByCategory = (categoryId, articleIds, entities) => {
  ReaderStore.update((s) => {
    s.entities = _.merge(s.entities, entities);
    s.articleIds.byCategory[categoryId] = _.uniq([
      ...(s.articleIds.byCategory[categoryId] || []),
      ...articleIds,
    ]);
  });
};

export const setArticles = (articleIds, entities) => {
  ReaderStore.update((s) => {
    s.entities = _.merge(s.entities, entities);
    s.articleIds.byAll["0"] = _.uniq([...(s.articleIds.byAll["0"] || []), ...articleIds]);
  });
};

export default ReaderStore;
