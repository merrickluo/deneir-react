import React from "react";

const ToggleTab = ({ tabs, selected, onSelect }) => {
  const selectedClasses = "bg-nord5 rounded-full";

  return (
    <div className="cursor-pointer w-full flex justify-between mt-8 bg-nord4 rounded-full text-caption font-light">
      {tabs.map((tab, i) => (
        <div
          key={i}
          className={`text-center w-full p-1 ${selected === i ? selectedClasses : ""}`}
          onClick={onSelect(i)}
        >
          <p>{tab.title}</p>
        </div>
      ))}
    </div>
  );
};

export default ToggleTab;
