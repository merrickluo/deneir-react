import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import PrivateRoute from "./PrivateRoute";
import Setup from "./Setup";
import Reader from "./Reader";

const App = () => {
  return (
    <Router>
      <div className="w-full bg-nord6">
        <Switch>
          <Route path="/setup" component={Setup} />
          <PrivateRoute
            path={["/categories/:categoryId", "/feeds/:feedId", "/"]}
            component={Reader}
          />
        </Switch>
      </div>
    </Router>
  );
};

export default App;
