import React, { useEffect, useState } from "react";

import MinifluxAPI from "api/miniflux";
import ReaderStore from "stores/ReaderStore";
import SvgIcon from "components/common/SvgIcon";
import FeedList from "components/FeedList";
import { Link, useRouteMatch, useParams } from "react-router-dom";

const Sidebar = ({ onSelect }) => {
  const categories = ReaderStore.useState((s) => s.entities.categories);
  const [expanded, setExpanded] = useState();

  useEffect(() => {
    MinifluxAPI.fetchCategories();
    MinifluxAPI.fetchFeeds();
  }, []);

  const handleExpand = (category) => () => {
    if (category === expanded) {
      setExpanded(null);
    } else {
      setExpanded(category);
    }
  };

  const { categoryId: selectedCategory } = useParams();

  const handleSelectAll = () => {
    onSelect("all", 0);
  };

  return (
    <>
      <div className="bg-nord2 pt-4 pl-4">
        <Link className="text-title pb-2 cursor-pointer" to="/">
          <p>Deneir Reader</p>
        </Link>
      </div>
      <div className="mt-2 cursor-pointer h-screen select-none overflow-auto">
        {Object.values(categories).map((category) => {
          const expand = expanded === category.id;
          const selected = selectedCategory === category.id;
          return (
            <div key={category.id}>
              <div
                className={`flex pl-3 items-center hover:bg-nord2 ${selected ? "bg-nord2" : ""}`}
              >
                <div className="p-1" onClick={handleExpand(category.id)}>
                  <SvgIcon
                    className={`w-2 transform ${expand ? "rotate-90" : ""}`}
                    name="angle-right-solid"
                  />
                </div>
                <Link to={`/categories/${category.id}`} className="ml-1 pl-2 py-1 w-full font-bold">
                  {category.title}
                </Link>
              </div>
              {expand && <FeedList categoryId={category.id} />}
            </div>
          );
        })}
      </div>
    </>
  );
};

export default Sidebar;
