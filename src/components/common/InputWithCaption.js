import React, { useState } from "react";

const InputWithCaption = ({ caption, value, className, onChange, type, validator }) => {
  const [errorMsg, setErrorMsg] = useState("");

  const handleChange = (event) => {
    let valid = false;
    const value = event.target.value;
    try {
      valid = validator(value);
      setErrorMsg("");
    } catch (error) {
      setErrorMsg(error.message);
    }
    onChange(valid, value);
  };

  return (
    <div className={`flex flex-col w-full ${className}`}>
      <div className="text-caption text-nord0 mb-1">{caption}</div>
      <input type={type || "text"} className="text-input" value={value} onChange={handleChange} />
      <div className={`mt-1 text-caption text-red ${errorMsg ? "visible" : "invisible"}`}>
        <p>{`${caption}: ${errorMsg}`}</p>
      </div>
    </div>
  );
};

export default InputWithCaption;
