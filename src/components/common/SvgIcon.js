import React, { useEffect, useState } from "react";

const SvgIcon = ({ name, className }) => {
  const [Icon, setIcon] = useState();

  useEffect(() => {
    const loadIcon = async () => {
      const icon = await import(/* webpackMode: "eager" */ `assets/icons/${name}.svg`);
      setIcon(<icon.default />);
    };

    loadIcon();
  }, [name]);

  return <div className={className}>{Icon}</div>;
};

export default SvgIcon;
