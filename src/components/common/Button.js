import React from "react";

const Button = ({ title, className, onClick }) => {
  return (
    <div className={`btn ${className}`} onClick={onClick}>
      {title}
    </div>
  );
};

export default Button;
