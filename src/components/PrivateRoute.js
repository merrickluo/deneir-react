import React from "react";
import { hasCredential } from "../storage/auth";
import { Route, Redirect } from "react-router-dom";

const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) =>
        hasCredential("miniflux") ? <Component {...props} /> : <Redirect to="/setup" />
      }
    />
  );
};

export default PrivateRoute;
