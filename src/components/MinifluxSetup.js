import React, { useEffect, useMemo, useState } from "react";

import Button from "components/common/Button";
import InputWithCaption from "components/common/InputWithCaption";
import ToggleTab from "components/ToggleTab";
import MinifluxAPI from "api/miniflux";
import Auth from "storage/auth";
import { urlValidator, emptyValidator } from "utils/validator";

const USE_PASSWORD = 0;
const USE_TOKEN = 1;

const MinifluxSetup = ({ onSetupSuccess }) => {
  const [currentMode, setCurrentMode] = useState(USE_TOKEN);
  const [errorMsg, setErrorMsg] = useState("");
  const [params, setParams] = useState({
    server: "https://",
    username: "",
    password: "",
    token: "",
  });

  const tabs = [{ title: "Use Pasword" }, { title: "Use Token" }];

  const handleSelectTab = (i) => () => setCurrentMode(i);
  const handleInput = (key) => (valid, value) => {
    setParams({ ...params, [key]: value });
  };
  const handleSetup = async () => {
    const credentials = { mode: currentMode, params };
    try {
      if (await MinifluxAPI.authorize(credentials)) {
        Auth.updateCredential("miniflux", credentials);
        onSetupSuccess();
      } else {
        setErrorMsg("failed to authorize");
      }
    } catch (error) {
      setErrorMsg(error.message);
    }
  };

  return (
    <>
      <ToggleTab tabs={tabs} selected={currentMode} onSelect={handleSelectTab} />
      <InputWithCaption
        caption="Server"
        className="mt-6"
        value={params.server}
        onChange={handleInput("server")}
        validator={urlValidator}
      />
      {currentMode === USE_PASSWORD && (
        <>
          <InputWithCaption
            className="mt-2"
            caption="Username"
            value={params.username}
            onChange={handleInput("username")}
          />
          <InputWithCaption
            className="mt-2"
            caption="Password"
            value={params.password}
            type="password"
            onChange={handleInput("password")}
          />
        </>
      )}
      {currentMode === USE_TOKEN && (
        <InputWithCaption
          caption="Token"
          value={params.token}
          className="mt-2"
          onChange={handleInput("token")}
          validator={emptyValidator}
        />
      )}

      <Button title="Start Read" className="mt-6 w-full" onClick={handleSetup} />
      <div className={`mt-1 text-caption text-red ${errorMsg ? "visible" : "invisible"}`}>
        <p>{errorMsg}</p>
      </div>
    </>
  );
};

export default MinifluxSetup;
