import React from "react";

import MinifluxSetup from "components/MinifluxSetup";
import { useHistory, useLocation } from "react-router-dom";
import { hasCredential } from "storage/auth";

const Setup = () => {
  const history = useHistory();
  const location = useLocation();
  const { from } = location.state || { from: { pathname: "/" } };

  const handleSetupSuccess = () => {
    history.replace(from);
  };

  return (
    <div className="p-6 w-full h-full flex flex-col justify-center items-center">
      <div className="w-72">
        <div className="flex justify-center text-title font-bold">
          <p className="text-nord0">Start read with</p>
          <p className="ml-1 text-nord8">Miniflux</p>
        </div>
        <MinifluxSetup onSetupSuccess={handleSetupSuccess} />
      </div>
    </div>
  );
};

export default Setup;
