import React, { useState } from "react";

import Sidebar from "components/Sidebar";
import ArticleList from "components/ArticleList";

const Reader = () => {
  return (
    <div className="flex w-full h-screen text-nord6 overflow-hidden">
      <div className="bg-nord3 w-64 min-w-64 h-screen">
        <Sidebar />
      </div>
      <div className="bg-nord6 h-screen w-full">
        <ArticleList />
      </div>
    </div>
  );
};

export default Reader;
