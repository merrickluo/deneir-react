import React from "react";

import ReaderStore from "stores/ReaderStore";
import SvgIcon from "components/common/SvgIcon";
import _ from "lodash";
import { Link, useParams } from "react-router-dom";

const FeedList = ({ categoryId }) => {
  const feeds = ReaderStore.useState(
    (s) => {
      const feedIds = s.feedIdsByCategory[categoryId];
      return _.compact(_.at(s.entities.feeds, feedIds));
    },
    [categoryId]
  );
  const { feedId: selectedFeedId } = useParams();

  return (
    <>
      {feeds &&
        feeds.map((feed) => (
          <Link
            key={feed.id}
            className={`pl-10 pt-1 py-1 hover:bg-nord2 flex items-center ${
              selectedFeedId == feed.id ? "bg-nord2" : ""
            }`}
            to={`/feeds/${feed.id}`}
          >
            <SvgIcon className="w-3" name="circle-regular" />
            <p className="font-light text-small pl-2 max-w-48 truncate whitespace-no-wrap">
              {feed.title}
            </p>
          </Link>
        ))}
    </>
  );
};

export default FeedList;
