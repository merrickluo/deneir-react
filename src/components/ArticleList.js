import React, { useState, useEffect, useRef } from "react";

import MinifluxAPI from "api/miniflux";
import ReaderStore from "stores/ReaderStore";
import _ from "lodash";

import Article from "components/Article";
import { useParams } from "react-router-dom";

const ArticleList = () => {
  const { categoryId, feedId } = useParams();

  const articleIds = ReaderStore.useState(
    (s) => {
      if (categoryId) {
        return s.articleIds.byCategory[categoryId];
      }
      if (feedId) {
        return s.articleIds.byFeed[feedId];
      }
      return s.articleIds.byAll[0];
    },
    [categoryId, feedId]
  );

  const title = ReaderStore.useState(
    (s) => {
      if (categoryId) {
        return s.entities.categories[categoryId]?.title;
      }
      if (feedId) {
        return s.entities.feeds[feedId]?.title;
      }
      return "All";
    },
    [categoryId, feedId]
  );

  useEffect(() => {
    MinifluxAPI.fetchArticles({ categoryId, feedId });
  }, [categoryId, feedId]);

  const [showArticleId, setShowArticleId] = useState();
  const handleOpenArticle = (articleId) => () => {
    setShowArticleId(articleId);
  };
  const handleCloseArticle = (articleId) => () => {
    setShowArticleId(null);
  };
  const handleScrollToArticle = (scrollTop) => {
    window.requestAnimationFrame(() => {
      listRef.current.scrollTop = scrollTop - 80;
    });
  };

  const [page, setPage] = useState(0);
  const [hasMore, setHasMore] = useState(true);
  const handleLoadMore = () => {
    MinifluxAPI.fetchArticles({ categoryId, feedId }, page + 1);
    setPage(page + 1);
  };

  const listRef = useRef();

  return (
    <div ref={listRef} className="w-full text-nord0 h-screen list overflow-auto">
      <div className="text-title sticky top-0 px-8 py-4 bg-nord6 border-b-1 border-nord3">
        <span className="text-nord8">Unread</span>
        <span className="ml-2">Articles in {title}</span>
      </div>
      {articleIds &&
        articleIds.map((articleId) => {
          return (
            <Article
              key={articleId}
              articleId={articleId}
              opened={showArticleId === articleId}
              onOpen={handleOpenArticle(articleId)}
              onClose={handleCloseArticle(articleId)}
              onScroll={handleScrollToArticle}
            />
          );
        })}
      <div
        className="flex justify-center items-center py-2 cursor-pointer"
        onClick={handleLoadMore}
      >
        <p>load more...</p>
      </div>
    </div>
  );
};

export default ArticleList;
