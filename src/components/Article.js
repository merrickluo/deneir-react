import React, { useRef } from "react";
import { DateTime } from "luxon";

import ReaderStore from "stores/ReaderStore";
import SvgIcon from "components/common/SvgIcon";

const ArticleCollapsed = ({ article, feed, onClick }) => {
  return (
    <div
      key={article.id}
      className="flex py-1 px-8 items-center border-nord4 border-b w-full cursor-pointer hover:bg-nord4"
      onClick={onClick}
    >
      <SvgIcon className="w-3 min-w-3 text-nord10" name="star-regular" />
      <p className="text-caption text-nord3 w-48 min-w-48 ml-2 truncate">{feed.title}</p>
      <p className="font-bold text-nord0 truncate ml-2 max-w-4xl">{article.title}</p>
      <p className="text-caption ml-auto">
        {DateTime.fromISO(article.published_at).toRelativeCalendar()}
      </p>
    </div>
  );
};

const Article = ({ articleId, opened, onOpen, onClose, onScroll }) => {
  const { article, feed, category } = ReaderStore.useState(
    (s) => {
      const article = s.entities.articles[articleId];
      const feed = s.entities.feeds[article.feed];
      const category = s.entities.categories[feed.category];
      return { article, feed, category };
    },
    [articleId]
  );

  const ref = useRef(null);

  const handleOpen = () => {
    onOpen();
    window.requestAnimationFrame(() => {
      onScroll(ref.current.offsetTop);
    });
  };

  if (!opened) {
    return <ArticleCollapsed article={article} feed={feed} onClick={handleOpen} />;
  }

  return (
    <div ref={ref} className="content px-8 pb-8 flex flex-col items-center justify-center">
      <div className="cursor-pointer h-8 w-full" onClick={onClose} />
      <div className="max-w-3xl w-full flex flex-col">
        <a href={article.url} target="_blank">
          <h1 className="article-title text-left text-nord0">{article.title} </h1>
        </a>
        <div className="flex pt-4 text-sm gray-400">
          <p>by {article.author} </p>
          <p className="pl-2">
            via <a href={feed.site_url}>{feed.title}</a>
          </p>
          {/* <p className="pl-2">at {article.published_at}</p> */}
          {/* <p class="pl-2">{category.title}</p> */}
        </div>
      </div>
      <article
        className="pt-8 max-w-3xl"
        dangerouslySetInnerHTML={{ __html: article.content }}
      ></article>
    </div>
  );
};

export default Article;
